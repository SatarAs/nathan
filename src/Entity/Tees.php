<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tees
 *
 * @ORM\Table(name="tees")
 * @ORM\Entity(repositoryClass="App\Repository\TeesRepository")
 */
class Tees
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=6)
     */
    private $color;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="unity", type="string", length=1)
     */
    private $unity;

    /**
     * @var int
     *
     * @ORM\Column(name="par", type="integer", nullable=true)
     */
    private $par;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=20, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=20, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="altitude", type="string", length=10, nullable=true)
     */
    private $altitude;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=12)
     */
    private $state;


    /*
     * Relationship
     * ========================================
     */

    /**
     * @ORM\ManyToOne(
     *      targetEntity="App\Entity\Holes",
     *      inversedBy="tee"
     * )
     */
    private $hole;


    /*
     * Constructor
     * ========================================
     */

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->state = "created";
    }


    /*
     * Getters-Setters
     * ========================================
     */

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tees
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Tees
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return Tees
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set unity
     *
     * @param string $unity
     *
     * @return Tees
     */
    public function setUnity($unity)
    {
        $this->unity = $unity;

        return $this;
    }

    /**
     * Get unity
     *
     * @return string
     */
    public function getUnity()
    {
        return $this->unity;
    }

    /**
     * Set par
     *
     * @param integer $par
     *
     * @return Tees
     */
    public function setPar($par)
    {
        $this->par = $par;

        return $this;
    }

    /**
     * Get par
     *
     * @return integer
     */
    public function getPar()
    {
        return $this->par;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Tees
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Tees
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set altitude
     *
     * @param string $altitude
     *
     * @return Tees
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;

        return $this;
    }

    /**
     * Get altitude
     *
     * @return string
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Tees
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set hole
     *
     * @param \App\Entity\Holes $hole
     *
     * @return Tees
     */
    public function setHole(\App\Entity\Holes $hole = null)
    {
        $this->hole = $hole;

        return $this;
    }

    /**
     * Get hole
     *
     * @return \App\Entity\Holes
     */
    public function getHole()
    {
        return $this->hole;
    }

}
