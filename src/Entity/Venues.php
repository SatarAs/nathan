<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Venues
 *
 * @ORM\Table(name="venues")
 * @ORM\Entity(repositoryClass="App\Repository\VenuesRepository")
 */
class Venues
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100, unique=true)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=45, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=45, nullable=true)
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=2)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=10, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=20, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=20, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="altitude", type="string", length=10, nullable=true)
     */
    private $altitude;

    /**
     * @var string
     *
     * @ORM\Column(name="unity", type="string", length=1)
     */
    private $unity;

    /**
     * @var int
     *
     * @ORM\Column(name="drivingRange", type="integer", nullable=true)
     */
    private $drivingRange;

    /**
     * @var int
     *
     * @ORM\Column(name="puttingGreen", type="integer", nullable=true)
     */
    private $puttingGreen;

    /**
     * @var int
     *
     * @ORM\Column(name="chippingGreen", type="integer", nullable=true)
     */
    private $chippingGreen;

    /**
     * @var int
     *
     * @ORM\Column(name="practiceBunker", type="integer", nullable=true)
     */
    private $practiceBunker;

    /**
     * @var bool
     *
     * @ORM\Column(name="motorCart", type="boolean")
     */
    private $motorCart;

    /**
     * @var bool
     *
     * @ORM\Column(name="pullCart", type="boolean")
     */
    private $pullCart;

    /**
     * @var bool
     *
     * @ORM\Column(name="golfClubsRental", type="boolean")
     */
    private $golfClubsRental;

    /**
     * @var bool
     *
     * @ORM\Column(name="clubFitting", type="boolean")
     */
    private $clubFitting;

    /**
     * @var bool
     *
     * @ORM\Column(name="proShop", type="boolean")
     */
    private $proShop;

    /**
     * @var bool
     *
     * @ORM\Column(name="golfLessons", type="boolean")
     */
    private $golfLessons;

    /**
     * @var bool
     *
     * @ORM\Column(name="caddieHire", type="boolean")
     */
    private $caddieHire;

    /**
     * @var bool
     *
     * @ORM\Column(name="restaurant", type="boolean")
     */
    private $restaurant;

    /**
     * @var bool
     *
     * @ORM\Column(name="receptionHall", type="boolean")
     */
    private $receptionHall;

    /**
     * @var bool
     *
     * @ORM\Column(name="clubHouse", type="boolean")
     */
    private $clubHouse;

    /**
     * @var bool
     *
     * @ORM\Column(name="changingRoom", type="boolean")
     */
    private $changingRoom;

    /**
     * @var bool
     *
     * @ORM\Column(name="lockers", type="boolean")
     */
    private $lockers;

    /**
     * @var bool
     *
     * @ORM\Column(name="lodging", type="boolean")
     */
    private $lodging;

    /**
     * @var bool
     *
     * @ORM\Column(name="nursery", type="boolean")
     */
    private $nursery;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=12)
     */
    private $state;


    /*
     * Relationship
     * ========================================
     */

    /**
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\VenuesContent",
     *      mappedBy="venue",
     *      cascade={"persist"}
     * )
     */
    private $content;

    /**
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\Courses",
     *      mappedBy="venue",
     *      cascade={"persist"}
     * )
     */
    private $courses;


    /*
     * Constructor
     * ========================================
     */



    /*
     * Getters-Setters
     * ========================================
     */

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
        $this->courses = new \Doctrine\Common\Collections\ArrayCollection();

        $this->motorCart = false;
        $this->pullCart = false;
        $this->golfClubsRental = false;
        $this->clubFitting = false;
        $this->proShop = false;
        $this->golfLessons = false;
        $this->caddieHire = false;
        $this->restaurant = false;
        $this->receptionHall = false;
        $this->clubHouse = false;
        $this->changingRoom = false;
        $this->lockers = false;
        $this->lodging = false;
        $this->nursery = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Venues
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Venues
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Venues
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Venues
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return Venues
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Venues
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Venues
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Venues
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Venues
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set altitude
     *
     * @param string $altitude
     *
     * @return Venues
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;

        return $this;
    }

    /**
     * Get altitude
     *
     * @return string
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * Set drivingRange
     *
     * @param integer $drivingRange
     *
     * @return Venues
     */
    public function setDrivingRange($drivingRange)
    {
        $this->drivingRange = $drivingRange;

        return $this;
    }

    /**
     * Get drivingRange
     *
     * @return integer
     */
    public function getDrivingRange()
    {
        return $this->drivingRange;
    }

    /**
     * Set puttingGreen
     *
     * @param integer $puttingGreen
     *
     * @return Venues
     */
    public function setPuttingGreen($puttingGreen)
    {
        $this->puttingGreen = $puttingGreen;

        return $this;
    }

    /**
     * Get puttingGreen
     *
     * @return integer
     */
    public function getPuttingGreen()
    {
        return $this->puttingGreen;
    }

    /**
     * Set chippingGreen
     *
     * @param integer $chippingGreen
     *
     * @return Venues
     */
    public function setChippingGreen($chippingGreen)
    {
        $this->chippingGreen = $chippingGreen;

        return $this;
    }

    /**
     * Get chippingGreen
     *
     * @return integer
     */
    public function getChippingGreen()
    {
        return $this->chippingGreen;
    }

    /**
     * Set practiceBunker
     *
     * @param integer $practiceBunker
     *
     * @return Venues
     */
    public function setPracticeBunker($practiceBunker)
    {
        $this->practiceBunker = $practiceBunker;

        return $this;
    }

    /**
     * Get practiceBunker
     *
     * @return integer
     */
    public function getPracticeBunker()
    {
        return $this->practiceBunker;
    }

    /**
     * Set motorCart
     *
     * @param boolean $motorCart
     *
     * @return Venues
     */
    public function setMotorCart($motorCart)
    {
        $this->motorCart = $motorCart;

        return $this;
    }

    /**
     * Get motorCart
     *
     * @return boolean
     */
    public function getMotorCart()
    {
        return $this->motorCart;
    }

    /**
     * Set pullCart
     *
     * @param boolean $pullCart
     *
     * @return Venues
     */
    public function setPullCart($pullCart)
    {
        $this->pullCart = $pullCart;

        return $this;
    }

    /**
     * Get pullCart
     *
     * @return boolean
     */
    public function getPullCart()
    {
        return $this->pullCart;
    }

    /**
     * Set golfClubsRental
     *
     * @param boolean $golfClubsRental
     *
     * @return Venues
     */
    public function setGolfClubsRental($golfClubsRental)
    {
        $this->golfClubsRental = $golfClubsRental;

        return $this;
    }

    /**
     * Get golfClubsRental
     *
     * @return boolean
     */
    public function getGolfClubsRental()
    {
        return $this->golfClubsRental;
    }

    /**
     * Set clubFitting
     *
     * @param boolean $clubFitting
     *
     * @return Venues
     */
    public function setClubFitting($clubFitting)
    {
        $this->clubFitting = $clubFitting;

        return $this;
    }

    /**
     * Get clubFitting
     *
     * @return boolean
     */
    public function getClubFitting()
    {
        return $this->clubFitting;
    }

    /**
     * Set proShop
     *
     * @param boolean $proShop
     *
     * @return Venues
     */
    public function setProShop($proShop)
    {
        $this->proShop = $proShop;

        return $this;
    }

    /**
     * Get proShop
     *
     * @return boolean
     */
    public function getProShop()
    {
        return $this->proShop;
    }

    /**
     * Set golfLessons
     *
     * @param boolean $golfLessons
     *
     * @return Venues
     */
    public function setGolfLessons($golfLessons)
    {
        $this->golfLessons = $golfLessons;

        return $this;
    }

    /**
     * Get golfLessons
     *
     * @return boolean
     */
    public function getGolfLessons()
    {
        return $this->golfLessons;
    }

    /**
     * Set caddieHire
     *
     * @param boolean $caddieHire
     *
     * @return Venues
     */
    public function setCaddieHire($caddieHire)
    {
        $this->caddieHire = $caddieHire;

        return $this;
    }

    /**
     * Get caddieHire
     *
     * @return boolean
     */
    public function getCaddieHire()
    {
        return $this->caddieHire;
    }

    /**
     * Set restaurant
     *
     * @param boolean $restaurant
     *
     * @return Venues
     */
    public function setRestaurant($restaurant)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant
     *
     * @return boolean
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Set receptionHall
     *
     * @param boolean $receptionHall
     *
     * @return Venues
     */
    public function setReceptionHall($receptionHall)
    {
        $this->receptionHall = $receptionHall;

        return $this;
    }

    /**
     * Get receptionHall
     *
     * @return boolean
     */
    public function getReceptionHall()
    {
        return $this->receptionHall;
    }

    /**
     * Set clubHouse
     *
     * @param boolean $clubHouse
     *
     * @return Venues
     */
    public function setClubHouse($clubHouse)
    {
        $this->clubHouse = $clubHouse;

        return $this;
    }

    /**
     * Get clubHouse
     *
     * @return boolean
     */
    public function getClubHouse()
    {
        return $this->clubHouse;
    }

    /**
     * Set changingRoom
     *
     * @param boolean $changingRoom
     *
     * @return Venues
     */
    public function setChangingRoom($changingRoom)
    {
        $this->changingRoom = $changingRoom;

        return $this;
    }

    /**
     * Get changingRoom
     *
     * @return boolean
     */
    public function getChangingRoom()
    {
        return $this->changingRoom;
    }

    /**
     * Set lockers
     *
     * @param boolean $lockers
     *
     * @return Venues
     */
    public function setLockers($lockers)
    {
        $this->lockers = $lockers;

        return $this;
    }

    /**
     * Get lockers
     *
     * @return boolean
     */
    public function getLockers()
    {
        return $this->lockers;
    }

    /**
     * Set lodging
     *
     * @param boolean $lodging
     *
     * @return Venues
     */
    public function setLodging($lodging)
    {
        $this->lodging = $lodging;

        return $this;
    }

    /**
     * Get lodging
     *
     * @return boolean
     */
    public function getLodging()
    {
        return $this->lodging;
    }

    /**
     * Set nursery
     *
     * @param boolean $nursery
     *
     * @return Venues
     */
    public function setNursery($nursery)
    {
        $this->nursery = $nursery;

        return $this;
    }

    /**
     * Get nursery
     *
     * @return boolean
     */
    public function getNursery()
    {
        return $this->nursery;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Venues
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Get content
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Add course
     *
     * @param \App\Entity\Courses $course
     *
     * @return Venues
     */
    public function addCourse(\App\Entity\Courses $course)
    {
        $this->courses[] = $course;
        $course->setVenue($this);
        return $this;
    }

    /**
     * Remove course
     *
     * @param \App\Entity\Courses $course
     */
    public function removeCourse(\App\Entity\Courses $course)
    {
        $this->courses->removeElement($course);
    }

    /**
     * Get courses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * Set unity
     *
     * @param string $unity
     *
     * @return Venues
     */
    public function setUnity($unity)
    {
        $this->unity = $unity;

        return $this;
    }

    /**
     * Get unity
     *
     * @return string
     */
    public function getUnity()
    {
        return $this->unity;
    }
}
