<?php

namespace App\Repository;

use App\Entity\Holes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Holes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Holes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Holes[]    findAll()
 * @method Holes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HolesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Holes::class);
    }

//    /**
//     * @return Holes[] Returns an array of Holes objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Holes
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
