<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HolesContent
 *
 * @ORM\Table(name="holes_content")
 * @ORM\Entity(repositoryClass="App\Repository\HolesContentRepository")
 */
class HolesContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /*
     * Relationship
     * ========================================
     */

    /**
     * @ORM\ManyToOne(
     *      targetEntity="App\Entity\Holes",
     *      inversedBy="content"
     * )
     */
    private $hole;

    /*
     * Constructor
     * ========================================
     */


    /*
     * Getters-Setters
     * ========================================
     */


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return HolesContent
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return HolesContent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hole
     *
     * @param \App\Entity\Holes $hole
     *
     * @return HolesContent
     */
    public function setHole(\App\Entity\Holes $hole = null)
    {
        $this->hole = $hole;

        return $this;
    }

    /**
     * Get hole
     *
     * @return \App\Entity\Holes
     */
    public function getHole()
    {
        return $this->hole;
    }
}

