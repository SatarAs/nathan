<?php

namespace App\Repository;

use App\Entity\Tees;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Tees|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tees|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tees[]    findAll()
 * @method Tees[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tees::class);
    }

//    /**
//     * @return Tees[] Returns an array of Tees objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tees
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
