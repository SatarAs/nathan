<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Holes
 *
 * @ORM\Table(name="holes")
 * @ORM\Entity(repositoryClass="App\Repository\HolesRepository")
 */
class Holes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="par", type="integer", nullable=true)
     */
    private $par;

    /**
     * @var int
     *
     * @ORM\Column(name="handicap", type="integer", nullable=true)
     */
    private $handicap;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rating", type="time", nullable=true)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=20, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=20, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="altitude", type="string", length=10, nullable=true)
     */
    private $altitude;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=12)
     */
    private $state;


    /*
     * Relationship
     * ========================================
     */

    /**
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\HolesContent",
     *      mappedBy="hole"
     * )
     */
    private $content;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="App\Entity\Courses",
     *      inversedBy="holes"
     * )
     */
    private $course;

    /**
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\Tees",
     *      mappedBy="hole"
     * )
     */
    private $tee;

    /*
     * Constructor
     * ========================================
     */



    /*
     * Getters-Setters
     * ========================================
     */

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tee = new \Doctrine\Common\Collections\ArrayCollection();
        $this->state = "created";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Holes
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Holes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set par
     *
     * @param integer $par
     *
     * @return Holes
     */
    public function setPar($par)
    {
        $this->par = $par;

        return $this;
    }

    /**
     * Get par
     *
     * @return integer
     */
    public function getPar()
    {
        return $this->par;
    }

    /**
     * Set handicap
     *
     * @param integer $handicap
     *
     * @return Holes
     */
    public function setHandicap($handicap)
    {
        $this->handicap = $handicap;

        return $this;
    }

    /**
     * Get handicap
     *
     * @return integer
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Holes
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Holes
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set altitude
     *
     * @param string $altitude
     *
     * @return Holes
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;

        return $this;
    }

    /**
     * Get altitude
     *
     * @return string
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Holes
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Add content
     *
     * @param \App\Entity\HolesContent $content
     *
     * @return Holes
     */
    public function addContent(\App\Entity\HolesContent $content)
    {
        $this->content[] = $content;

        return $this;
    }

    /**
     * Remove content
     *
     * @param \App\Entity\HolesContent $content
     */
    public function removeContent(\App\Entity\HolesContent $content)
    {
        $this->content->removeElement($content);
    }

    /**
     * Get content
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set course
     *
     * @param \App\Entity\Courses $course
     *
     * @return Holes
     */
    public function setCourse(\App\Entity\Courses $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \App\Entity\Courses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Add tee
     *
     * @param \App\Entity\Tees $tee
     *
     * @return Holes
     */
    public function addTee(\App\Entity\Tees $tee)
    {
        $this->tee[] = $tee;

        return $this;
    }

    /**
     * Remove tee
     *
     * @param \App\Entity\Tees $tee
     */
    public function removeTee(\App\Entity\Tees $tee)
    {
        $this->tee->removeElement($tee);
    }

    /**
     * Get tee
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTee()
    {
        return $this->tee;
    }

    /**
     * Set rating
     *
     * @param \DateTime $rating
     *
     * @return Holes
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return \DateTime
     */
    public function getRating()
    {
        return $this->rating;
    }
}
