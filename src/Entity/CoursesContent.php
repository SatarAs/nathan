<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * CoursesContent
 *
 * @ORM\Table(name="courses_content")
 * @ORM\Entity(repositoryClass="App\Repository\CoursesContentRepository")
 */
class CoursesContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /*
     * Relationship
     * ========================================
     */

    /**
     * @ORM\ManyToOne(
     *      targetEntity="App\Entity\Courses",
     *      inversedBy="content"
     * )
     */
    private $course;



    /*
     * Constructor
     * ========================================
     */



    /*
     * Getters-Setters
     * ========================================
     */


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return CoursesContent
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CoursesContent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set course
     *
     * @param \App\Entity\Courses $course
     *
     * @return CoursesContent
     */
    public function setCourse(\App\Entity\Courses $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \App\Entity\Courses
     */
    public function getCourse()
    {
        return $this->course;
    }
}
