<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VenuesContent
 *
 * @ORM\Table(name="venues_content")
 * @ORM\Entity(repositoryClass="App\Repository\VenuesContentRepository")
 */
class VenuesContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="text", nullable=true)
     */
    private $caption;


    /*
     * Relationship
     * ========================================
     */

    /**
     * @ORM\ManyToOne(
     *      targetEntity="App\Entity\Venues",
     *      inversedBy="content"
     * )
     */
    private $venue;



    /*
     * Constructor
     * ========================================
     */



    /*
     * Getters-Setters
     * ========================================
     */


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return VenuesContent
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return VenuesContent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set caption
     *
     * @param string $caption
     *
     * @return VenuesContent
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set venue
     *
     * @param \App\Entity\Venues $venue
     *
     * @return VenuesContent
     */
    public function setVenue(\App\Entity\Venues $venue = null)
    {
        $this->venue = $venue;

        return $this;
    }

    /**
     * Get venue
     *
     * @return \App\Entity\Venues
     */
    public function getVenue()
    {
        return $this->venue;
    }
}
