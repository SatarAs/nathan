<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Courses
 *
 * @ORM\Table(name="courses")
 * @ORM\Entity(repositoryClass="App\Repository\CoursesRepository")
 */
class Courses
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=20, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=20, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="altitude", type="string", length=10, nullable=true)
     */
    private $altitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="openingDate", type="date", nullable=true)
     */
    private $openingDate;

    /**
     * @var int
     *
     * @ORM\Column(name="holesTotal", type="integer", nullable=true)
     */
    private $holesTotal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="roundtripRatingGo", type="time", nullable=true)
     */
    private $roundtripRatingGo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="roundtripRatingBack", type="time", nullable=true)
     */
    private $roundtripRatingBack;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="roundtripRatingTotal", type="time", nullable=true)
     */
    private $roundtripRatingTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="sss_male", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $sssMale;

    /**
     * @var string
     *
     * @ORM\Column(name="sss_female", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $sssFemale;

    /**
     * @var int
     *
     * @ORM\Column(name="slope_male", type="integer", nullable=true)
     */
    private $slopeMale;

    /**
     * @var int
     *
     * @ORM\Column(name="slope_female", type="integer", nullable=true)
     */
    private $slopeFemale;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=12)
     */
    private $state;


    /*
     * Relationship
     * ========================================
     */

    /**
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\CoursesContent",
     *      mappedBy="course",
     *      cascade={"all"}
     * )
     */
    private $content;

    /**
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\Holes",
     *      mappedBy="course",
     *      cascade={"all"}
     * )
     */
    private $holes;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="App\Entity\Venues",
     *      inversedBy="courses"
     * )
     */
    private $venue;


    /*
     * Constructor
     * ========================================
     */

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->holesTotal = 0;
        $this->sssMale = NULL;
        $this->sssFemale = NULL;
        $this->slopeMale = NULL;
        $this->slopeFemale = NULL;
        $this->state = "created";
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
        $this->holes = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /*
     * Getters-Setters
     * ========================================
     */

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Courses
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Courses
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Courses
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Courses
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set altitude
     *
     * @param string $altitude
     *
     * @return Courses
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;

        return $this;
    }

    /**
     * Get altitude
     *
     * @return string
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * Set openingDate
     *
     * @param \DateTime $openingDate
     *
     * @return Courses
     */
    public function setOpeningDate($openingDate)
    {
        $this->openingDate = $openingDate;

        return $this;
    }

    /**
     * Get openingDate
     *
     * @return \DateTime
     */
    public function getOpeningDate()
    {
        return $this->openingDate;
    }

    /**
     * Set holesTotal
     *
     * @param integer $holesTotal
     *
     * @return Courses
     */
    public function setHolesTotal($holesTotal)
    {
        $this->holesTotal = $holesTotal;

        return $this;
    }

    /**
     * Get holesTotal
     *
     * @return integer
     */
    public function getHolesTotal()
    {
        return $this->holesTotal;
    }

    /**
     * Set roundtripRatingGo
     *
     * @param \DateTime $roundtripRatingGo
     *
     * @return Courses
     */
    public function setRoundtripRatingGo($roundtripRatingGo)
    {
        $this->roundtripRatingGo = $roundtripRatingGo;

        return $this;
    }

    /**
     * Get roundtripRatingGo
     *
     * @return \DateTime
     */
    public function getRoundtripRatingGo()
    {
        return $this->roundtripRatingGo;
    }

    /**
     * Set roundtripRatingBack
     *
     * @param \DateTime $roundtripRatingBack
     *
     * @return Courses
     */
    public function setRoundtripRatingBack($roundtripRatingBack)
    {
        $this->roundtripRatingBack = $roundtripRatingBack;

        return $this;
    }

    /**
     * Get roundtripRatingBack
     *
     * @return \DateTime
     */
    public function getRoundtripRatingBack()
    {
        return $this->roundtripRatingBack;
    }

    /**
     * Set roundtripRatingTotal
     *
     * @param \DateTime $roundtripRatingTotal
     *
     * @return Courses
     */
    public function setRoundtripRatingTotal($roundtripRatingTotal)
    {
        $this->roundtripRatingTotal = $roundtripRatingTotal;

        return $this;
    }

    /**
     * Get roundtripRatingTotal
     *
     * @return \DateTime
     */
    public function getRoundtripRatingTotal()
    {
        return $this->roundtripRatingTotal;
    }

    /**
     * Set sssMale
     *
     * @param string $sssMale
     *
     * @return Courses
     */
    public function setSssMale($sssMale)
    {
        $this->sssMale = $sssMale;

        return $this;
    }

    /**
     * Get sssMale
     *
     * @return string
     */
    public function getSssMale()
    {
        return $this->sssMale;
    }

    /**
     * Set sssFemale
     *
     * @param string $sssFemale
     *
     * @return Courses
     */
    public function setSssFemale($sssFemale)
    {
        $this->sssFemale = $sssFemale;

        return $this;
    }

    /**
     * Get sssFemale
     *
     * @return string
     */
    public function getSssFemale()
    {
        return $this->sssFemale;
    }

    /**
     * Set slopeMale
     *
     * @param integer $slopeMale
     *
     * @return Courses
     */
    public function setSlopeMale($slopeMale)
    {
        $this->slopeMale = $slopeMale;

        return $this;
    }

    /**
     * Get slopeMale
     *
     * @return integer
     */
    public function getSlopeMale()
    {
        return $this->slopeMale;
    }

    /**
     * Set slopeFemale
     *
     * @param integer $slopeFemale
     *
     * @return Courses
     */
    public function setSlopeFemale($slopeFemale)
    {
        $this->slopeFemale = $slopeFemale;

        return $this;
    }

    /**
     * Get slopeFemale
     *
     * @return integer
     */
    public function getSlopeFemale()
    {
        return $this->slopeFemale;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Courses
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Add content
     *
     * @param \App\Entity\CoursesContent $content
     *
     * @return Courses
     */
    public function addContent(\App\Entity\CoursesContent $content)
    {
        $this->content[] = $content;

        return $this;
    }

    /**
     * Remove content
     *
     * @param \App\Entity\CoursesContent $content
     */
    public function removeContent(\App\Entity\CoursesContent $content)
    {
        $this->content->removeElement($content);
    }

    /**
     * Get content
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Add hole
     *
     * @param \App\Entity\Holes $hole
     *
     * @return Courses
     */
    public function addHole(\App\Entity\Holes $hole)
    {
        $this->holes[] = $hole;
        $hole->setCourse($this);
        return $this;
    }

    /**
     * Remove hole
     *
     * @param \App\Entity\Holes $hole
     */
    public function removeHole(\App\Entity\Holes $hole)
    {
        $this->holes->removeElement($hole);
    }

    /**
     * Get holes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHoles()
    {
        return $this->holes;
    }

    /**
     * Set venue
     *
     * @param \App\Entity\Venues $venue
     *
     * @return Courses
     */
    public function setVenue(\App\Entity\Venues $venue = null)
    {
        $this->venue = $venue;

        return $this;
    }

    /**
     * Get venue
     *
     * @return \App\Entity\Venues
     */
    public function getVenue()
    {
        return $this->venue;
    }

}
