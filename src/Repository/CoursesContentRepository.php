<?php

namespace App\Repository;

use App\Entity\CoursesContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CoursesContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoursesContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoursesContent[]    findAll()
 * @method CoursesContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursesContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CoursesContent::class);
    }

//    /**
//     * @return CoursesContent[] Returns an array of CoursesContent objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CoursesContent
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
