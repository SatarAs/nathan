<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180413120533 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses (id INT AUTO_INCREMENT NOT NULL, venue_id INT DEFAULT NULL, slug VARCHAR(100) NOT NULL, name VARCHAR(100) NOT NULL, latitude VARCHAR(20) DEFAULT NULL, longitude VARCHAR(20) DEFAULT NULL, altitude VARCHAR(10) DEFAULT NULL, openingDate DATE DEFAULT NULL, holesTotal INT DEFAULT NULL, roundtripRatingGo TIME DEFAULT NULL, roundtripRatingBack TIME DEFAULT NULL, roundtripRatingTotal TIME DEFAULT NULL, sss_male NUMERIC(5, 2) DEFAULT NULL, sss_female NUMERIC(5, 2) DEFAULT NULL, slope_male INT DEFAULT NULL, slope_female INT DEFAULT NULL, state VARCHAR(12) NOT NULL, INDEX IDX_A9A55A4C40A73EBA (venue_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_content (id INT AUTO_INCREMENT NOT NULL, course_id INT DEFAULT NULL, language VARCHAR(5) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_47FCCD35591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE holes (id INT AUTO_INCREMENT NOT NULL, course_id INT DEFAULT NULL, number INT NOT NULL, name VARCHAR(100) DEFAULT NULL, par INT DEFAULT NULL, handicap INT DEFAULT NULL, rating TIME DEFAULT NULL, latitude VARCHAR(20) DEFAULT NULL, longitude VARCHAR(20) DEFAULT NULL, altitude VARCHAR(10) DEFAULT NULL, state VARCHAR(12) NOT NULL, INDEX IDX_9C6EA1E4591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE holes_content (id INT AUTO_INCREMENT NOT NULL, hole_id INT DEFAULT NULL, language VARCHAR(5) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_4AC386AE15ADE12C (hole_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tees (id INT AUTO_INCREMENT NOT NULL, hole_id INT DEFAULT NULL, name VARCHAR(100) DEFAULT NULL, color VARCHAR(6) NOT NULL, value INT DEFAULT NULL, unity VARCHAR(1) NOT NULL, par INT DEFAULT NULL, latitude VARCHAR(20) DEFAULT NULL, longitude VARCHAR(20) DEFAULT NULL, altitude VARCHAR(10) DEFAULT NULL, state VARCHAR(12) NOT NULL, INDEX IDX_5A835E7815ADE12C (hole_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE venues (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(100) NOT NULL, name VARCHAR(100) NOT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(45) DEFAULT NULL, area VARCHAR(45) DEFAULT NULL, country VARCHAR(2) NOT NULL, postalCode VARCHAR(10) DEFAULT NULL, latitude VARCHAR(20) DEFAULT NULL, longitude VARCHAR(20) DEFAULT NULL, altitude VARCHAR(10) DEFAULT NULL, unity VARCHAR(1) NOT NULL, drivingRange INT DEFAULT NULL, puttingGreen INT DEFAULT NULL, chippingGreen INT DEFAULT NULL, practiceBunker INT DEFAULT NULL, motorCart TINYINT(1) NOT NULL, pullCart TINYINT(1) NOT NULL, golfClubsRental TINYINT(1) NOT NULL, clubFitting TINYINT(1) NOT NULL, proShop TINYINT(1) NOT NULL, golfLessons TINYINT(1) NOT NULL, caddieHire TINYINT(1) NOT NULL, restaurant TINYINT(1) NOT NULL, receptionHall TINYINT(1) NOT NULL, clubHouse TINYINT(1) NOT NULL, changingRoom TINYINT(1) NOT NULL, lockers TINYINT(1) NOT NULL, lodging TINYINT(1) NOT NULL, nursery TINYINT(1) NOT NULL, state VARCHAR(12) NOT NULL, UNIQUE INDEX UNIQ_652E22AD989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE venues_content (id INT AUTO_INCREMENT NOT NULL, venue_id INT DEFAULT NULL, language VARCHAR(5) NOT NULL, description LONGTEXT DEFAULT NULL, caption LONGTEXT DEFAULT NULL, INDEX IDX_8F93974740A73EBA (venue_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C40A73EBA FOREIGN KEY (venue_id) REFERENCES venues (id)');
        $this->addSql('ALTER TABLE courses_content ADD CONSTRAINT FK_47FCCD35591CC992 FOREIGN KEY (course_id) REFERENCES courses (id)');
        $this->addSql('ALTER TABLE holes ADD CONSTRAINT FK_9C6EA1E4591CC992 FOREIGN KEY (course_id) REFERENCES courses (id)');
        $this->addSql('ALTER TABLE holes_content ADD CONSTRAINT FK_4AC386AE15ADE12C FOREIGN KEY (hole_id) REFERENCES holes (id)');
        $this->addSql('ALTER TABLE tees ADD CONSTRAINT FK_5A835E7815ADE12C FOREIGN KEY (hole_id) REFERENCES holes (id)');
        $this->addSql('ALTER TABLE venues_content ADD CONSTRAINT FK_8F93974740A73EBA FOREIGN KEY (venue_id) REFERENCES venues (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_content DROP FOREIGN KEY FK_47FCCD35591CC992');
        $this->addSql('ALTER TABLE holes DROP FOREIGN KEY FK_9C6EA1E4591CC992');
        $this->addSql('ALTER TABLE holes_content DROP FOREIGN KEY FK_4AC386AE15ADE12C');
        $this->addSql('ALTER TABLE tees DROP FOREIGN KEY FK_5A835E7815ADE12C');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C40A73EBA');
        $this->addSql('ALTER TABLE venues_content DROP FOREIGN KEY FK_8F93974740A73EBA');
        $this->addSql('DROP TABLE courses');
        $this->addSql('DROP TABLE courses_content');
        $this->addSql('DROP TABLE holes');
        $this->addSql('DROP TABLE holes_content');
        $this->addSql('DROP TABLE tees');
        $this->addSql('DROP TABLE venues');
        $this->addSql('DROP TABLE venues_content');
    }
}
