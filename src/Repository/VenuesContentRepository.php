<?php

namespace App\Repository;

use App\Entity\VenuesContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VenuesContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method VenuesContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method VenuesContent[]    findAll()
 * @method VenuesContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VenuesContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VenuesContent::class);
    }

//    /**
//     * @return VenuesContent[] Returns an array of VenuesContent objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VenuesContent
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
